from typing import Callable, Union, Tuple, List, NoReturn, Optional
from datetime import timedelta, datetime
from ..options import *
from ....framework import *


DEFAULT_INTERFACE = ... # type: str
LOOPBACK_INTERFACE = ... # type: str


class BufferRef:
    pass


class PacketInfo:
    origin = ... # type: str
    receive_time = ... # type: datetime
    read_time = ... # type: datetime
    def since(self) -> Tuple[timedelta, timedelta]: ...


class UdpListenerTransport:
    def __init__(self,
                 dispatcher: Dispatcher,
                 groups: Union[str, Tuple[str, int], Tuple[List[str], int]],
                 receive_callback: Callable[[UdpListenerTransport, BufferRef], NoReturn],
                 interface: str = DEFAULT_INTERFACE,
                 options: UdpOptions = UdpOptions(),
                 error_callback: Optional[Callable[[Exception], NoReturn]] = None) -> NoReturn: ...

    info = ... # type: PacketInfo

    def __bool__(self) -> bool: ...
    def __enter__(self) -> UdpListenerTransport: ...
    def __exit__(self, exc_type, exc_val, exc_tb) -> NoReturn: ...
    def __str__(self) -> str: ...


class UdpPublisherTransport:
    def __init__(self,
                 dispatcher: Dispatcher,
                 group: Union[str, Tuple[str, int]],
                 interface: str=DEFAULT_INTERFACE,
                 options: UdpOptions=UdpOptions(),
                 error_callback: Optional[Callable[[Exception], NoReturn]]=None) -> NoReturn: ...

    has_pending = ... # type: bool

    def send(self, data: Union[str, bytes, memoryview]) -> bool: ...
    def wait(self, timeout: Union[float, timedelta]) -> bool: ...

    def __bool__(self) -> bool: ...
    def __enter__(self) -> UdpPublisherTransport: ...
    def __exit__(self, exc_type, exc_val, exc_tb) -> NoReturn: ...
    def __str__(self) -> str: ...


class TcpClientTransport:
    def __init__(self,
                 dispatcher: Dispatcher,
                 options: TcpOptions,
                 endpoint: Union[str, Tuple[str, int]],
                 connected_callback: Callable[[TcpClientTransport], NoReturn],
                 disconnected_callback: Callable[[TcpClientTransport], NoReturn],
                 receive_callback: Callable[[TcpClientTransport, BufferRef], NoReturn],
                 error_callback: Optional[Callable[[Exception], NoReturn]]) -> NoReturn: ...

    info = ... # type: PacketInfo
    has_pending = ... # type: bool

    def send(self, data: Union[str, bytes, memoryview]) -> bool: ...
    def wait(self, timeout: Union[float, timedelta]) -> bool: ...
    def disconnect(self) -> None: ...

    def __bool__(self) -> bool: ...
    def __enter__(self) -> TcpClientTransport: ...
    def __exit__(self, exc_type, exc_val, exc_tb) -> NoReturn: ...
    def __str__(self) -> str: ...


class TcpServerTransport:
    def __init__(self,
                 dispatcher: Dispatcher,
                 options: TcpOptions,
                 endpoint: Union[str, Tuple[str, int]],
                 connected_callback: Callable[[TcpConnectedClientTransport], NoReturn],
                 disconnected_callback: Callable[[TcpConnectedClientTransport], NoReturn],
                 receive_callback: Callable[[TcpConnectedClientTransport, BufferRef], NoReturn],
                 error_callback: Optional[Callable[[Exception], NoReturn]]) -> None: ...

    at = ... # type: Tuple[str, int]

    def __bool__(self) -> bool: ...
    def __enter__(self) -> TcpServerTransport: ...
    def __exit__(self, exc_type, exc_val, exc_tb) -> NoReturn: ...
    def __str__(self) -> str: ...


class TcpConnectedClientTransport:
    id = ... # type: int
    info = ... # type: PacketInfo
    has_pending = ... # type: bool
    closure = ... # type: object

    def send(self, data: Union[str, bytes, memoryview]) -> bool: ...
    def wait(self, timeout: Union[float, timedelta]) -> bool: ...
    def disconnect(self) -> None: ...

    def __bool__(self) -> bool: ...
    def __enter__(self) -> TcpConnectedClientTransport: ...
    def __exit__(self, exc_type, exc_val, exc_tb) -> NoReturn: ...
    def __str__(self) -> str: ...


class UdpClientTransport:
    def __init__(self,
                 dispatcher: Dispatcher,
                 endpoint: Union[str, Tuple[str, int]],
                 receive_callback: Callable[[UdpClientTransport, BufferRef], NoReturn],
                 options: UdpOptions=UdpOptions(),
                 error_callback: Union[None, Callable[[Exception], NoReturn]]=None) -> None: ...

    info = ... # type: PacketInfo
    has_pending = ... # type: bool

    def send(self, data: Union[str, bytes, memoryview]) -> bool: ...
    def wait(self, timeout: Union[float, timedelta]) -> bool: ...

    def __bool__(self) -> bool: ...
    def __enter__(self) -> UdpClientTransport: ...
    def __exit__(self, exc_type, exc_val, exc_tb) -> NoReturn: ...
    def __str__(self) -> str: ...


class UdpServerTransport:
    def __init__(self,
                 dispatcher: Dispatcher,
                 endpoint: Union[str, Tuple[str, int]],
                 receive_callback: Callable[[UdpServerTransport, BufferRef], NoReturn],
                 options: UdpOptions=UdpOptions(),
                 error_callback: Union[None, Callable[[Exception], NoReturn]]=None) -> None: ...

    at = ... # type: Tuple[str, int]
    has_pending = ... # type: bool

    def reply(self, data: Union[str, bytes, memoryview]) -> bool: ...
    def wait(self, timeout: Union[float, timedelta]) -> bool: ...

    def __bool__(self) -> bool: ...
    def __enter__(self) -> UdpServerTransport: ...
    def __exit__(self, exc_type, exc_val, exc_tb) -> NoReturn: ...
    def __str__(self) -> str: ...