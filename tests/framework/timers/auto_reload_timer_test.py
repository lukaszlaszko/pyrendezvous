from hamcrest import *
from datetime import timedelta
from time import sleep
from framework import *


class Receiver:
    def __init__(self):
        self.elapsed_count = 0
        self.total_count = 0
        self.error = None

    def on_elapsed(self, source, count):
        self.elapsed_count += 1
        self.total_count += count

    def on_error(self, error):
        self.error = error


def test_create_with_none_dispatcher():
    assert_that(calling(AutoReloadTimer).with_args(None, 1.0, lambda s, c: _, lambda e: _),
                raises(TypeError))


def test_create_with_timedelta_interval():
    receiver = Receiver()

    dispatcher = Dispatcher(receiver.on_error)
    auto_reload_timer = AutoReloadTimer(dispatcher,
                                        timedelta(seconds=1),
                                        receiver.on_elapsed,
                                        receiver.on_error)

    assert_that(receiver.elapsed_count, is_(0))
    assert_that(receiver.total_count, is_(0))
    assert_that(receiver.error, is_(None))


def test_create_with_float_interval():
    receiver = Receiver()

    dispatcher = Dispatcher(receiver.on_error)
    auto_reload_timer = AutoReloadTimer(dispatcher,
                                        1.0,
                                        receiver.on_elapsed,
                                        receiver.on_error)

    assert_that(receiver.elapsed_count, is_(0))
    assert_that(receiver.total_count, is_(0))
    assert_that(receiver.error, is_(None))


def test_create_with_none_interval():
    receiver = Receiver()

    dispatcher = Dispatcher(receiver.on_error)
    assert_that(calling(AutoReloadTimer).with_args(dispatcher, None, receiver.on_elapsed, receiver.on_error),
                raises(TypeError))


def test_create_with_none_elapsed_callback():
    receiver = Receiver()

    dispatcher = Dispatcher(receiver.on_error)
    assert_that(calling(AutoReloadTimer).with_args(dispatcher, 1.0, None, receiver.on_error),
                raises(TypeError))


def test_create_with_default_error_callback():
    receiver = Receiver()

    dispatcher = Dispatcher(receiver.on_error)
    auto_reload_timer = AutoReloadTimer(dispatcher,
                                        1.0,
                                        receiver.on_elapsed)


def test_arm():
    receiver = Receiver()

    dispatcher = Dispatcher(receiver.on_error)
    auto_reload_timer = AutoReloadTimer(dispatcher,
                                        timedelta(milliseconds=200),
                                        receiver.on_elapsed,
                                        receiver.on_error)

    assert_that(receiver.elapsed_count, is_(0))
    assert_that(receiver.total_count, is_(0))
    assert_that(receiver.error, is_(None))

    auto_reload_timer.arm()
    sleep(0.5)

    assert_that(receiver.elapsed_count, is_(0))
    assert_that(receiver.total_count, is_(0))
    assert_that(receiver.error, is_(None))

    dispatcher.dispatch()

    assert_that(receiver.elapsed_count, is_(greater_than(0)))
    assert_that(receiver.total_count, is_(greater_than(0)))
    assert_that(receiver.error, is_(None))


def test_disarm():
    receiver = Receiver()

    dispatcher = Dispatcher(receiver.on_error)
    auto_reload_timer = AutoReloadTimer(dispatcher,
                                        timedelta(seconds=20),
                                        receiver.on_elapsed,
                                        receiver.on_error)

    assert_that(receiver.elapsed_count, is_(0))
    assert_that(receiver.total_count, is_(0))
    assert_that(receiver.error, is_(None))

    auto_reload_timer.arm()
    sleep(0.5)
    auto_reload_timer.disarm()

    assert_that(receiver.elapsed_count, is_(0))
    assert_that(receiver.total_count, is_(0))
    assert_that(receiver.error, is_(None))

    dispatcher.dispatch()

    assert_that(receiver.elapsed_count, is_(0))
    assert_that(receiver.total_count, is_(0))
    assert_that(receiver.error, is_(None))


def test_is_armed_after_create():
    receiver = Receiver()

    dispatcher = Dispatcher(receiver.on_error)
    auto_reload_timer = AutoReloadTimer(dispatcher,
                                        timedelta(milliseconds=200),
                                        receiver.on_elapsed,
                                        receiver.on_error)

    assert_that(auto_reload_timer.is_armed, is_(False))


def test_is_armed_after_arm():
    receiver = Receiver()

    dispatcher = Dispatcher(receiver.on_error)
    auto_reload_timer = AutoReloadTimer(dispatcher,
                                        timedelta(milliseconds=200),
                                        receiver.on_elapsed,
                                        receiver.on_error)

    assert_that(auto_reload_timer.is_armed, is_(False))

    auto_reload_timer.arm()
    assert_that(auto_reload_timer.is_armed, is_(True))


def test_is_armed_after_disarm():
    receiver = Receiver()

    dispatcher = Dispatcher(receiver.on_error)
    auto_reload_timer = AutoReloadTimer(dispatcher,
                                        timedelta(milliseconds=200),
                                        receiver.on_elapsed,
                                        receiver.on_error)

    auto_reload_timer.arm()
    assert_that(auto_reload_timer.is_armed, is_(True))

    auto_reload_timer.disarm()
    assert_that(auto_reload_timer.is_armed, is_(False))
