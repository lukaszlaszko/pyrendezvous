from hamcrest import *
from datetime import timedelta
from time import sleep
from framework import *


class Receiver(object):
    def __init__(self):
        self.count = 0
        self.error = None

    def on_elapsed(self, timer):
        self.count += 1

    def on_error(self, error):
        self.error = error


def test_create_no_dispatcher():
    assert_that(calling(OneShotTimer).with_args(None, lambda source: _, lambda error: _),
                raises(TypeError))


def test_create_no_elapsed_callback():
    dispatcher = Dispatcher(lambda error: _)
    assert_that(calling(OneShotTimer).with_args(dispatcher, None, lambda error: _),
                raises(TypeError))


def test_schedule_with_timedelta():
    receiver = Receiver()

    dispatcher = Dispatcher(receiver.on_error)
    one_shot_timer = OneShotTimer(dispatcher, receiver.on_elapsed, receiver.on_error)
    one_shot_timer.schedule(timedelta(milliseconds=200))

    sleep(0.5)

    assert_that(receiver.count, is_(0))
    assert_that(receiver.error, is_(None))

    dispatcher.dispatch()

    assert_that(receiver.count, is_(1))
    assert_that(receiver.error, is_(None))


def test_schedule_with_float():
    receiver = Receiver()

    dispatcher = Dispatcher(receiver.on_error)
    one_shot_timer = OneShotTimer(dispatcher, receiver.on_elapsed, receiver.on_error)
    one_shot_timer.schedule(0.2)

    sleep(0.5)

    assert_that(receiver.count, is_(0))
    assert_that(receiver.error, is_(None))

    dispatcher.dispatch()

    assert_that(receiver.count, is_(1))
    assert_that(receiver.error, is_(None))


def test_schedule_with_none():
    receiver = Receiver()

    dispatcher = Dispatcher(receiver.on_error)
    one_shot_timer = OneShotTimer(dispatcher, receiver.on_elapsed, receiver.on_error)
    assert_that(calling(one_shot_timer.schedule).with_args(None), raises(TypeError))


def test_schedule_error_in_elapsed_callaback():
    def raise_error(source):
        raise Exception('test error')

    receiver = Receiver()

    dispatcher = Dispatcher()
    one_shot_timer = OneShotTimer(dispatcher, raise_error, receiver.on_error)

    one_shot_timer.schedule(0.1)

    sleep(0.5)
    assert_that(calling(dispatcher.dispatch), raises(Exception))


def test_schedule_already_scheduled():
    receiver = Receiver()

    dispatcher = Dispatcher(receiver.on_error)
    one_shot_timer = OneShotTimer(dispatcher, receiver.on_elapsed, receiver.on_error)
    one_shot_timer.schedule(timedelta(seconds=20))
    sleep(0.5)

    assert_that(receiver.count, is_(0))

    one_shot_timer.schedule(timedelta(milliseconds=200))
    sleep(0.5)
    assert_that(receiver.count, is_(0))

    dispatcher.dispatch()
    assert_that(receiver.count, is_(1))


def test_is_armed_after_create():
    receiver = Receiver()

    dispatcher = Dispatcher(receiver.on_error)
    one_shot_timer = OneShotTimer(dispatcher, receiver.on_elapsed, receiver.on_error)

    assert_that(one_shot_timer.is_armed, is_(False))


def test_is_armed_after_schedule():
    receiver = Receiver()

    dispatcher = Dispatcher(receiver.on_error)
    one_shot_timer = OneShotTimer(dispatcher, receiver.on_elapsed, receiver.on_error)
    one_shot_timer.schedule(timedelta(seconds=50))

    assert_that(one_shot_timer.is_armed, is_(True))


def test_disarm_scheduled():
    receiver = Receiver()

    dispatcher = Dispatcher(receiver.on_error)
    one_shot_timer = OneShotTimer(dispatcher, receiver.on_elapsed, receiver.on_error)
    one_shot_timer.schedule(timedelta(seconds=50))

    one_shot_timer.disarm()
    assert_that(one_shot_timer.is_armed, is_(False))