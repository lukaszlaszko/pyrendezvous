from hamcrest import *
from datetime import timedelta
from framework import Dispatcher
from framework.transports import UdpOptions
from framework.transports.ipv4 import UdpClientTransport
from pysyscall_intercept import SysCallInterceptor, SYS_SENDTO
from socket import socket, AF_INET, EAGAIN, IPPROTO_UDP, MSG_DONTWAIT, SOCK_DGRAM


def test_create_with_endpoint_string():
    dispatcher = Dispatcher()
    options = UdpOptions()

    with UdpClientTransport(dispatcher,
                            '127.0.0.1:20000',
                            lambda t, d: _,
                            options) as transport:
        assert_that(bool(transport), is_(True))
        assert_that(transport.info, is_not(None))
        assert_that(transport.info.origin, is_(('127.0.0.1', 20000)))


def test_create_with_endpoint_tuple():
    dispatcher = Dispatcher()
    options = UdpOptions()

    with UdpClientTransport(dispatcher,
                            ('127.0.0.1', 20000),
                            lambda t, d: _,
                            options) as transport:
        assert_that(bool(transport), is_(True))
        assert_that(transport.info, is_not(None))
        assert_that(transport.info.origin, is_(('127.0.0.1', 20000)))


def test_create_with_endpoint_none():
    dispatcher = Dispatcher()
    options = UdpOptions()

    assert_that(calling(UdpClientTransport).with_args(dispatcher,
                                                      options,
                                                      None,
                                                      lambda t, d: _),
                raises(TypeError))


def test_create__all_defaults():
    dispatcher = Dispatcher()

    transport = UdpClientTransport(dispatcher, '127.0.0.1:20000', lambda t, d: _)
    assert_that(transport, is_not(None))


def test_create__custom_options():
    dispatcher = Dispatcher()
    options = UdpOptions()

    transport = UdpClientTransport(dispatcher, '127.0.0.1:20000', lambda t, d: _, options)
    assert_that(transport, is_not(None))


def test_resource_manager():
    dispatcher = Dispatcher()
    options = UdpOptions()

    with UdpClientTransport(dispatcher,
                            '127.0.0.1:20000',
                            lambda t, d: _,
                            options) as transport:
        assert_that(bool(transport), is_(True))

    assert_that(bool(transport), is_(False))


def test_send_with_string():
    with socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP) as s:
        s.bind(('127.0.0.1', 0))
        server_address = s.getsockname()

        dispatcher = Dispatcher()
        with UdpClientTransport(dispatcher, server_address, lambda s, d: _) as transport:
            transport.send('abcd')
            dispatcher.dispatch()

            received = s.recv(1024, MSG_DONTWAIT)
            assert_that(received, is_not(None))
            assert_that(received.decode('UTF-8'), is_(equal_to('abcd')))


def test_send_with_buffer():
    with socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP) as s:
        s.bind(('127.0.0.1', 0))
        server_address = s.getsockname()

        dispatcher = Dispatcher()
        with UdpClientTransport(dispatcher, server_address, lambda s, d: _) as transport:
            transport.send(b'1234567')
            dispatcher.dispatch()

            received = s.recv(1024, MSG_DONTWAIT)
            assert_that(received, is_not(None))
            assert_that(received, is_(equal_to(b'1234567')))


def test_send_with_none():
    with socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP) as s:
        s.bind(('127.0.0.1', 0))
        server_address = s.getsockname()

        dispatcher = Dispatcher()
        with UdpClientTransport(dispatcher, server_address, lambda s, d: _) as transport:
            assert_that(calling(transport.send).with_args(None), raises(TypeError))


def test_wait__timeout():
    with socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP) as s:
        s.bind(('127.0.0.1', 0))
        server_address = s.getsockname()

        dispatcher = Dispatcher()
        with UdpClientTransport(dispatcher, server_address, lambda s, d: _) as transport: # type: UdpClientTransport
            assert_that(transport.wait(timedelta(milliseconds=200)), is_(True))

            def on_intercept():
                return -EAGAIN

            with SysCallInterceptor(SYS_SENDTO, on_intercept):
                transport.send(b'abcd')
                assert_that(transport.has_pending, is_(True))
                assert_that(transport.wait(timedelta(milliseconds=200)), is_(False))


def test_receive_reply():
    with socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP) as s:
        s.bind(('127.0.0.1', 0))
        server_address = s.getsockname()

        class Receiver:
            def __init__(self):
                self.received = []

            def on_receive(self, source, data):
                self.received.append(bytes(data))

        receiver = Receiver()

        dispatcher = Dispatcher()
        with UdpClientTransport(dispatcher, server_address, receiver.on_receive) as transport:
            transport.send(b'1234567')
            dispatcher.dispatch()

            _, tr_endpoint = s.recvfrom(1024, MSG_DONTWAIT)
            s.sendto(b'9876', MSG_DONTWAIT, tr_endpoint)
            dispatcher.dispatch()
            assert_that(receiver.received, is_not(empty()))
            assert_that(receiver.received, has_length(1))
            assert_that(receiver.received[0], is_(equal_to(b'9876')))
