from hamcrest import *
from datetime import timedelta
from framework import Dispatcher
from framework.transports import TcpOptions
from framework.transports.ipv4 import TcpClientTransport
from pysyscall_intercept import SysCallInterceptor, SYS_SENDTO
from socket import socket, AF_INET, EAGAIN, IPPROTO_TCP, MSG_DONTWAIT, SOCK_STREAM


def test_create__with_endpoint_string():
    dispatcher = Dispatcher()
    options = TcpOptions()

    transport = TcpClientTransport(dispatcher,
                                   "127.0.0.1:30000",
                                   lambda t: _,
                                   lambda t: _,
                                   lambda t, b: _,
                                   options)
    assert_that(transport, is_not(None))


def test_create__with_endpoint_tuple():
    dispatcher = Dispatcher()
    options = TcpOptions()

    transport = TcpClientTransport(dispatcher,
                                   ("127.0.0.1", 30000),
                                   lambda t: _,
                                   lambda t: _,
                                   lambda t, b: _,
                                   options)
    assert_that(transport, is_not(None))


def test_create__with_endpoint_none():
    dispatcher = Dispatcher()
    options = TcpOptions()

    assert_that(calling(TcpClientTransport).with_args(dispatcher,
                                                      None,
                                                      lambda t: _,
                                                      lambda t: _,
                                                      lambda t, b: _,
                                                      options),
                raises(TypeError))


def test_create__all_defaults():
    dispatcher = Dispatcher()
    transport = TcpClientTransport(dispatcher,
                                   ("127.0.0.1", 30000),
                                   lambda t: _,
                                   lambda t: _,
                                   lambda t, b: _)

    assert_that(transport, is_not(None))


def test_connect():
    with socket(AF_INET, SOCK_STREAM, IPPROTO_TCP) as s:
        s.bind(('127.0.0.1', 0))
        server_address = s.getsockname()
        s.listen(1)

        dispatcher = Dispatcher()

        class Observer:
            def __init__(self):
                self.connected_count = 0
                self.disconnected_count = 0

            def on_connected(self, source):
                self.connected_count += 1

            def on_disconnected(self, source):
                self.disconnected_count += 1

        observer = Observer()
        with TcpClientTransport(dispatcher,
                                server_address,
                                observer.on_connected,
                                observer.on_disconnected,
                                lambda c, d: _) as transport:
            assert_that(bool(transport), is_(False))
            assert_that(observer.connected_count, is_(0))
            assert_that(observer.disconnected_count, is_(0))

            dispatcher.dispatch()
            assert_that(bool(transport), is_(True))
            assert_that(observer.connected_count, is_(1))
            assert_that(observer.disconnected_count, is_(0))

        assert_that(bool(transport), is_(False))
        assert_that(observer.connected_count, is_(1))
        assert_that(observer.disconnected_count, is_(0)) # no disconnection notification if client is closed


def test_connect_remote_disconnect():
    with socket(AF_INET, SOCK_STREAM, IPPROTO_TCP) as s:
        s.bind(('127.0.0.1', 0))
        server_address = s.getsockname()
        s.listen(1)

        dispatcher = Dispatcher()

        class Observer:
            def __init__(self):
                self.connected_count = 0
                self.disconnected_count = 0

            def on_connected(self, source):
                self.connected_count += 1

            def on_disconnected(self, source):
                self.disconnected_count += 1

        observer = Observer()
        with TcpClientTransport(dispatcher,
                                server_address,
                                observer.on_connected,
                                observer.on_disconnected,
                                lambda c, d: _) as transport:
            assert_that(observer.connected_count, is_(0))
            assert_that(observer.disconnected_count, is_(0))

            dispatcher.dispatch()
            assert_that(observer.connected_count, is_(1))
            assert_that(observer.disconnected_count, is_(0))

            s.close()
            dispatcher.dispatch()
            assert_that(observer.connected_count, is_(1))
            assert_that(observer.disconnected_count, is_(1))  # disconnectio notice if remote shutdown the connection


def test_receive_from_server():
    with socket(AF_INET, SOCK_STREAM, IPPROTO_TCP) as s:
        s.bind(('127.0.0.1', 0))
        server_address = s.getsockname()
        s.listen(1)

        dispatcher = Dispatcher()

        class Observer:
            def __init__(self):
                self.connected = False
                self.received = []

            def on_connected(self, source):
                self.connected = True

            def on_disconnected(self, source):
                self.connected = False

            def on_receive(self, source, data):
                self.received.append(bytes(data))


        observer = Observer()
        with TcpClientTransport(dispatcher,
                                server_address,
                                observer.on_connected,
                                observer.on_disconnected,
                                observer.on_receive) as transport:
            dispatcher.dispatch()
            assert_that(observer.connected, is_(True))

            clientsocket, address = s.accept()
            clientsocket.send(b'1234')

            dispatcher.dispatch()
            assert_that(observer.received, is_not(empty()))
            assert_that(observer.received, has_length(1))
            assert_that(observer.received[0], is_(equal_to(b'1234')))


def test_send_string():
    with socket(AF_INET, SOCK_STREAM, IPPROTO_TCP) as s:
        s.bind(('127.0.0.1', 0))
        server_address = s.getsockname()
        s.listen(1)

        dispatcher = Dispatcher()

        class Observer:
            def __init__(self):
                self.connected = False

            def on_connected(self, source):
                self.connected = True

            def on_disconnected(self, source):
                self.connected = False


        observer = Observer()
        with TcpClientTransport(dispatcher,
                                server_address,
                                observer.on_connected,
                                observer.on_disconnected,
                                lambda s, d: _) as transport:
            dispatcher.dispatch()
            assert_that(observer.connected, is_(True))

            clientsocket, address = s.accept()

            transport.send('abcd')
            dispatcher.dispatch()

            received = clientsocket.recv(1024, MSG_DONTWAIT)
            assert_that(received, is_not(None))
            assert_that(received.decode('UTF-8'), is_(equal_to('abcd')))


def test_send_buffer():
    with socket(AF_INET, SOCK_STREAM, IPPROTO_TCP) as s:
        s.bind(('127.0.0.1', 0))
        server_address = s.getsockname()
        s.listen(1)

        dispatcher = Dispatcher()

        class Observer:
            def __init__(self):
                self.connected = False

            def on_connected(self, source):
                self.connected = True

            def on_disconnected(self, source):
                self.connected = False


        observer = Observer()
        with TcpClientTransport(dispatcher,
                                server_address,
                                observer.on_connected,
                                observer.on_disconnected,
                                lambda s, d: _) as transport:
            dispatcher.dispatch()
            assert_that(observer.connected, is_(True))

            clientsocket, address = s.accept()

            transport.send(b'1234567')
            dispatcher.dispatch()

            received = clientsocket.recv(1024, MSG_DONTWAIT)
            assert_that(received, is_not(None))
            assert_that(received, is_(equal_to(b'1234567')))


def test_disconnect():
    with socket(AF_INET, SOCK_STREAM, IPPROTO_TCP) as s:
        s.bind(('127.0.0.1', 0))
        server_address = s.getsockname()
        s.listen(1)

        dispatcher = Dispatcher()
        options = TcpOptions()

        dispatcher = Dispatcher()

        class Observer:
            def __init__(self):
                self.connected = False

            def on_connected(self, source):
                self.connected = True

            def on_disconnected(self, source):
                self.connected = False

        observer = Observer()
        transport = TcpClientTransport(dispatcher,
                                       server_address,
                                       observer.on_connected,
                                       observer.on_disconnected,
                                       lambda s, d: _)

        dispatcher.dispatch()
        assert_that(bool(transport), is_(True))

        transport.disconnect()
        assert_that(bool(transport), is_(False))


def test_wait__no_pending():
    with socket(AF_INET, SOCK_STREAM, IPPROTO_TCP) as s:
        s.bind(('127.0.0.1', 0))
        server_address = s.getsockname()
        s.listen(1)

        dispatcher = Dispatcher()
        options = TcpOptions()

        dispatcher = Dispatcher()

        class Observer:
            def __init__(self):
                self.connected = False

            def on_connected(self, source):
                self.connected = True

            def on_disconnected(self, source):
                self.connected = False

        observer = Observer()
        with TcpClientTransport(dispatcher,
                                server_address,
                                observer.on_connected,
                                observer.on_disconnected,
                                lambda s, d: _) as transport:
            assert_that(transport.wait(timedelta(milliseconds=500)), is_(True))


def test_wait__timeout():
    with socket(AF_INET, SOCK_STREAM, IPPROTO_TCP) as s:
        s.bind(('127.0.0.1', 0))
        server_address = s.getsockname()
        s.listen(1)

        dispatcher = Dispatcher()
        options = TcpOptions()

        dispatcher = Dispatcher()

        class Observer:
            def __init__(self):
                self.connected = False

            def on_connected(self, source):
                self.connected = True

            def on_disconnected(self, source):
                self.connected = False

        def on_send_to():
            return -EAGAIN

        observer = Observer()
        with TcpClientTransport(dispatcher,
                                server_address,
                                observer.on_connected,
                                observer.on_disconnected,
                                lambda s, d: _) as transport:
            with SysCallInterceptor(SYS_SENDTO, on_send_to):
                transport.send(b'abc')
                assert_that(transport.wait(timedelta(milliseconds=500)), is_(False))

