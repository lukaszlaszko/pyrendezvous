from hamcrest import *
from datetime import timedelta
from framework import Dispatcher
from framework.transports import TcpOptions
from framework.transports.ipv4 import TcpServerTransport
from socket import socket, AF_INET, IPPROTO_TCP, MSG_DONTWAIT, SOCK_STREAM


def test_create__with_endpoint_string():
    dispatcher = Dispatcher()
    options = TcpOptions()
    transport = TcpServerTransport(dispatcher,
                                   '127.0.0.1:0',
                                   lambda: _,
                                   lambda: _,
                                   lambda: _,
                                   options)

    assert_that(transport.at, is_not(None))


def test_create__with_endpoint_tuple():
    dispatcher = Dispatcher()
    options = TcpOptions()
    transport = TcpServerTransport(dispatcher,
                                   ('127.0.0.1', 0),
                                   lambda: _,
                                   lambda: _,
                                   lambda: _,
                                   options)

    assert_that(transport.at, is_not(None))


def test_create__with_endpoint_none():
    dispatcher = Dispatcher()
    options = TcpOptions()

    assert_that(calling(TcpServerTransport).with_args(dispatcher,
                                                      None,
                                                      lambda: _,
                                                      lambda: _,
                                                      lambda: _,
                                                      options),
                raises(TypeError))


def test_create__with_error_callback():
    dispatcher = Dispatcher()
    options = TcpOptions()
    transport = TcpServerTransport(dispatcher,
                                   "127.0.0.1:0",
                                   lambda: _,
                                   lambda: _,
                                   lambda: _,
                                   options,
                                   lambda e: _)

    assert_that(transport.at, is_not(None))


def test_create__on_existing_port():
    dispatcher = Dispatcher()
    options = TcpOptions()

    transport_1 = TcpServerTransport(dispatcher,
                                     "127.0.0.1:0",
                                     lambda: _,
                                     lambda: _,
                                     lambda: _,
                                     options,
                                     lambda e: _)

    assert_that(calling(TcpServerTransport).with_args(dispatcher, transport_1.at, lambda: _, lambda: _, lambda: _, options),
                raises(RuntimeError))


def test_create__default_options():
    dispatcher = Dispatcher()

    transport = TcpServerTransport(dispatcher,
                                   "127.0.0.1:0",
                                   lambda: _,
                                   lambda: _,
                                   lambda: _)

    assert_that(transport.at, is_not(None))


def test_resource_manager():
    dispatcher = Dispatcher()
    with TcpServerTransport(dispatcher, ('127.0.0.1', 0), lambda: _, lambda: _, lambda: _) as transport:
        assert_that(bool(transport), is_(True))
    assert_that(bool(transport), is_(False))


def test_connection_from_client():
    class Observer:
        def __init__(self):
            self.connected_count = 0
            self.disconnected_count = 0
            self.clients = []

        def on_connected(self, client):
            self.connected_count += 1
            self.clients.append(client)

        def on_disconnected(self, client):
            self.disconnected_count += 1

    observer = Observer()

    dispatcher = Dispatcher()
    with TcpServerTransport(dispatcher,
                            '127.0.0.1:0',
                            observer.on_connected,
                            observer.on_disconnected,
                            lambda c, b: _) as transport:

        with socket(AF_INET, SOCK_STREAM, IPPROTO_TCP) as s:
            s.connect(transport.at)
            assert_that(observer.connected_count, is_(0))
            assert_that(observer.disconnected_count, is_(0))

            dispatcher.dispatch()
            assert_that(observer.connected_count, is_(1))
            assert_that(observer.disconnected_count, is_(0))

        # disconnection
        dispatcher.dispatch()
        assert_that(observer.connected_count, is_(1))
        assert_that(observer.disconnected_count, is_(1))


def test_connected_client_receive():
    class Receiver:
        def __init__(self):
            self.connected = False
            self.received = []

        def on_connected(self, client):
            self.connected = True

        def on_disconnected(self, client):
            self.connected = False

        def on_receive(self, client, data):
            self.received.append(bytes(data))


    receiver = Receiver()

    dispatcher = Dispatcher()
    with TcpServerTransport(dispatcher,
                            '127.0.0.1:0',
                            receiver.on_connected,
                            receiver.on_disconnected,
                            receiver.on_receive) as transport:

        with socket(AF_INET, SOCK_STREAM, IPPROTO_TCP) as s:
            s.connect(transport.at)

            dispatcher.dispatch()
            assert_that(receiver.connected, is_(True))
            assert_that(receiver.received, is_(empty()))

            s.send(b'1234')
            dispatcher.dispatch()
            assert_that(receiver.received, is_not(empty()))
            assert_that(receiver.received, has_length(1))
            assert_that(receiver.received[0], is_(equal_to(b'1234')))

        # disconnection
        dispatcher.dispatch()
        assert_that(receiver.connected, is_(False))


def test_connected_client_send_string():
    class Receiver:
        def __init__(self):
            self.client = None  # type: TcpConnectedClientTransport

        def on_connected(self, client):
            self.client = client

        def on_disconnected(self, client):
            self.client = None

    observer = Receiver()

    dispatcher = Dispatcher()
    with TcpServerTransport(dispatcher,
                            '127.0.0.1:0',
                            observer.on_connected,
                            observer.on_disconnected,
                            lambda s, d: _) as transport:
        with socket(AF_INET, SOCK_STREAM, IPPROTO_TCP) as s:
            s.connect(transport.at)

            dispatcher.dispatch()
            assert_that(observer.client, is_not(None))

            observer.client.send('abcd')
            dispatcher.dispatch()

            received = s.recv(1024, MSG_DONTWAIT)
            assert_that(received, is_not(None))
            assert_that(received.decode('UTF-8'), is_(equal_to('abcd')))


def test_connected_client_send_buffer():
    class Receiver:
        def __init__(self):
            self.client = None  # type: TcpConnectedClientTransport

        def on_connected(self, client):
            self.client = client

        def on_disconnected(self, client):
            self.client = None

    observer = Receiver()

    dispatcher = Dispatcher()
    with TcpServerTransport(dispatcher,
                            '127.0.0.1:0',
                            observer.on_connected,
                            observer.on_disconnected,
                            lambda s, d: _) as transport:
        with socket(AF_INET, SOCK_STREAM, IPPROTO_TCP) as s:
            s.connect(transport.at)

            dispatcher.dispatch()
            assert_that(observer.client, is_not(None))

            observer.client.send(b'123489')
            dispatcher.dispatch()

            received = s.recv(1024, MSG_DONTWAIT)
            assert_that(received, is_not(None))
            assert_that(received, is_(equal_to(b'123489')))


def test_connected_client_wait():
    class Receiver:
        def __init__(self):
            self.client = None  # type: TcpConnectedClientTransport

        def on_connected(self, client):
            self.client = client

        def on_disconnected(self, client):
            self.client = None

    observer = Receiver()

    dispatcher = Dispatcher()
    with TcpServerTransport(dispatcher,
                            '127.0.0.1:0',
                            observer.on_connected,
                            observer.on_disconnected,
                            lambda s, d: _) as transport:
        with socket(AF_INET, SOCK_STREAM, IPPROTO_TCP) as s:
            s.connect(transport.at)

            dispatcher.dispatch()
            assert_that(observer.client, is_not(None))
            assert_that(observer.client.wait(timedelta(milliseconds=200)), is_(True))

            observer.client.send(b'123')
            assert_that(observer.client.wait(timedelta(milliseconds=200)), is_(True))

            dispatcher.dispatch()
            assert_that(observer.client.wait(timedelta(milliseconds=200)), is_(True))


def test_connected_client_disconnect():
    class Receiver:
        def __init__(self):
            self.client = None  # type: TcpConnectedClientTransport

        def on_connected(self, client):
            self.client = client

        def on_disconnected(self, client):
            self.client = None

    observer = Receiver()

    dispatcher = Dispatcher()
    with TcpServerTransport(dispatcher,
                            '127.0.0.1:0',
                            observer.on_connected,
                            observer.on_disconnected,
                            lambda s, d: _) as transport:
        with socket(AF_INET, SOCK_STREAM, IPPROTO_TCP) as s:
            s.connect(transport.at)

            dispatcher.dispatch()
            assert_that(observer.client, is_not(None))
            assert_that(bool(observer.client), is_(True))

            observer.client.disconnect()
            assert_that(bool(observer.client), is_(False))
            assert_that(observer.client, is_not(None)) # disconnected callback not invoked
