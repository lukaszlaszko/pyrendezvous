from hamcrest import *
from datetime import timedelta
from framework import Dispatcher
from framework.transports import UdpOptions
from framework.transports.ipv4 import UdpPublisherTransport, DEFAULT_INTERFACE, LOOPBACK_INTERFACE
from pysyscall_intercept import SysCallInterceptor, SYS_SENDTO
from pytest import fixture
from socket import inet_aton, socket, AF_INET, EAGAIN, IP_ADD_MEMBERSHIP, IPPROTO_UDP, MSG_DONTWAIT, SOCK_DGRAM, SOL_IP


@fixture()
def multicast_group_1():
    return '224.0.56.56'


@fixture()
def multicast_group_2():
    return '224.0.56.57'


@fixture()
def multicast_port():
    return 34567


@fixture()
def multicast_address_and_port_1(multicast_group_1, multicast_port):
    return multicast_group_1, multicast_port


@fixture()
def multicast_address_and_port_2(multicast_group_2, multicast_port):
    return multicast_group_2, multicast_port


def test_create__with_group_as_string(multicast_group_1, multicast_port):
    dispatcher = Dispatcher()
    options = UdpOptions()
    transport = UdpPublisherTransport(dispatcher,
                                      f'{multicast_group_1}:{multicast_port}',
                                      DEFAULT_INTERFACE,
                                      options)

    assert_that(transport, is_not(None))


def test_create__with_group_as_tuple(multicast_group_1, multicast_port):
    dispatcher = Dispatcher()
    options = UdpOptions()
    transport = UdpPublisherTransport(dispatcher,
                                      (multicast_group_1, multicast_port),
                                      LOOPBACK_INTERFACE,
                                      options)

    assert_that(transport, is_not(None))


def test_create__wrong_group_definition():
    dispatcher = Dispatcher()
    assert_that(calling(UdpPublisherTransport).with_args(dispatcher, 123), raises(TypeError))
    assert_that(calling(UdpPublisherTransport).with_args(dispatcher, 'abc'), raises(ValueError))
    assert_that(calling(UdpPublisherTransport).with_args(dispatcher, '224.0.0.1'), raises(ValueError))


def test_create__all_defaults():
    dispatcher = Dispatcher()
    transport = UdpPublisherTransport(dispatcher, "224.0.0.1:5000")

    assert_that(transport, is_not(None))


def test_create__default_interface():
    dispatcher = Dispatcher()
    options = UdpOptions()
    transport = UdpPublisherTransport(dispatcher, "224.0.0.1:5000", options=options)

    assert_that(transport, is_not(None))


def test_send_buffer(multicast_group_1, multicast_port):
    # test subscriber
    with socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP) as s:
        s.bind((multicast_group_1, multicast_port))
        s.setsockopt(SOL_IP, IP_ADD_MEMBERSHIP,
                     inet_aton(multicast_group_1) + inet_aton("127.0.0.1"))

        dispatcher = Dispatcher()
        with UdpPublisherTransport(dispatcher,
                                   (multicast_group_1, multicast_port),
                                   LOOPBACK_INTERFACE) as transport:

            data = b'1234'
            transport.send(data)
            dispatcher.dispatch()

            received = s.recv(MSG_DONTWAIT)
            assert_that(received, is_not(None))
            assert_that(received, is_(equal_to(b'1234')))


def test_send_memory_view(multicast_group_1, multicast_port):
    # test subscriber
    with socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP) as s:
        s.bind((multicast_group_1, multicast_port))
        s.setsockopt(SOL_IP, IP_ADD_MEMBERSHIP,
                     inet_aton(multicast_group_1) + inet_aton("127.0.0.1"))

        dispatcher = Dispatcher()
        with UdpPublisherTransport(dispatcher,
                                   (multicast_group_1, multicast_port),
                                   "127.0.0.1") as transport:

            data = memoryview(b'1234')
            transport.send(data)
            dispatcher.dispatch()

            received = s.recv(MSG_DONTWAIT)
            assert_that(received, is_not(None))
            assert_that(received, is_(equal_to(b'1234')))


def test_send_string(multicast_group_1, multicast_port):
    # test subscriber
    with socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP) as s:
        s.bind((multicast_group_1, multicast_port))
        s.setsockopt(SOL_IP, IP_ADD_MEMBERSHIP,
                     inet_aton(multicast_group_1) + inet_aton("127.0.0.1"))

        dispatcher = Dispatcher()
        options = UdpOptions()
        with UdpPublisherTransport(dispatcher,
                                   (multicast_group_1, multicast_port),
                                   "127.0.0.1") as transport:

            data = "1234"
            transport.send(data)
            dispatcher.dispatch()

            received = s.recv(MSG_DONTWAIT)
            assert_that(received, is_not(None))
            assert_that(received, is_(equal_to(b'1234')))


def test_send_none(multicast_group_1, multicast_port):
    dispatcher = Dispatcher()
    options = UdpOptions()
    transport = UdpPublisherTransport(dispatcher,
                                      (multicast_group_1, multicast_port),
                                      "127.0.0.1")

    assert_that(calling(transport.send).with_args(None), raises(TypeError))


def test_resource_manager(multicast_group_1, multicast_port):
    dispatcher = Dispatcher()
    options = UdpOptions()
    group = (multicast_group_1, multicast_port)
    with UdpPublisherTransport(dispatcher, group, "127.0.0.1") as transport:
        assert_that(bool(transport), is_(True))

    assert_that(bool(transport), is_(False))


def test_wait__no_pending(multicast_address_and_port_1):
    dispatcher = Dispatcher()
    with UdpPublisherTransport(dispatcher, multicast_address_and_port_1, interface='127.0.0.1') as transport:
        assert_that(transport.wait(timedelta(milliseconds=500)), is_(True))


def test_wait__timeout(multicast_address_and_port_1):
    def on_send_to():
        return -EAGAIN

    dispatcher = Dispatcher()
    with UdpPublisherTransport(dispatcher, multicast_address_and_port_1, interface='127.0.0.1') as transport:
        with SysCallInterceptor(SYS_SENDTO, on_send_to):
            transport.send(b'abc')
            assert_that(transport.wait(timedelta(milliseconds=500)), is_(False))
