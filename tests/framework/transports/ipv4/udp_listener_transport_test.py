from hamcrest import *
from datetime import timedelta
from pytest import fixture
from framework import Dispatcher
from framework.transports import UdpOptions
from framework.transports.ipv4 import UdpListenerTransport, DEFAULT_INTERFACE, LOOPBACK_INTERFACE
from socket import socket, AF_INET, SOCK_DGRAM, IPPROTO_UDP, IPPROTO_IP, IP_MULTICAST_TTL


@fixture()
def multicast_group_1():
    return '224.0.56.56'


@fixture()
def multicast_group_2():
    return '224.0.56.57'


@fixture()
def multicast_port():
    return 34567


@fixture()
def multicast_address_and_port_1(multicast_group_1, multicast_port):
    return multicast_group_1, multicast_port


@fixture()
def multicast_address_and_port_2(multicast_group_2, multicast_port):
    return multicast_group_2, multicast_port


def test_create_with_group_string():
    dispatcher = Dispatcher()
    options = UdpOptions()
    transport = UdpListenerTransport(dispatcher, "224.0.0.1:23000", lambda s, d: _, "127.0.0.1", options)

    assert_that(transport, is_not(None))


def test_create_with_group_tuple(multicast_address_and_port_1):
    dispatcher = Dispatcher()
    options = UdpOptions()
    transport = UdpListenerTransport(dispatcher, multicast_address_and_port_1, lambda s, d: _, "127.0.0.1", options)

    assert_that(transport, is_not(None))


def test_create__with_all_defaults(multicast_address_and_port_1):
    dispatcher = Dispatcher()
    transport = UdpListenerTransport(dispatcher, multicast_address_and_port_1, lambda s, d: _)

    assert_that(transport, is_not(None))


def test_create_with_multiple_groups(multicast_group_1, multicast_group_2, multicast_port):
    dispatcher = Dispatcher()
    options = UdpOptions()
    transport = UdpListenerTransport(dispatcher,
                                     ([multicast_group_1, multicast_group_2], multicast_port),
                                     lambda s, d: _,
                                     LOOPBACK_INTERFACE,
                                     options)

    assert_that(transport, is_not(None))


def test_create_with_default_interface(multicast_group_1, multicast_group_2, multicast_port):
    dispatcher = Dispatcher()
    options = UdpOptions()
    transport = UdpListenerTransport(dispatcher,
                                     ([multicast_group_1, multicast_group_2], multicast_port),
                                     lambda s, d: _,
                                     DEFAULT_INTERFACE,
                                     options)

    assert_that(transport, is_not(None))


def test_listen_single_group(multicast_address_and_port_1):
    class Receiver(object):
        def __init__(self):
            self.count = 0
            self.received = []

        def on_received(self, source, data):
            # type: (UdpListenerTransport, BufferRef) -> None
            self.count += 1
            self.received.append(bytes(data))

    receiver = Receiver()

    dispatcher = Dispatcher()
    options = UdpOptions()
    transport = UdpListenerTransport(dispatcher,
                                     multicast_address_and_port_1,
                                     receiver.on_received,
                                     DEFAULT_INTERFACE,
                                     options)

    # test publisher
    s = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)
    s.setsockopt(IPPROTO_IP, IP_MULTICAST_TTL, 0)

    # send test data
    send_buffer = bytearray([1, 2, 3, 4])
    s.sendto(send_buffer, multicast_address_and_port_1)

    assert_that(receiver.count, is_(0))

    dispatcher.dispatch()
    assert_that(receiver.count, is_(1))


def test_listen_multiple_groups(multicast_group_1,
                                multicast_group_2,
                                multicast_port,
                                multicast_address_and_port_1,
                                multicast_address_and_port_2):
    class Receiver(object):
        def __init__(self):
            self.count = 0
            self.received = []

        def on_received(self, source, data):
            # type: (UdpListenerTransport, BufferRef) -> None
            self.count += 1
            self.received.append((source.info.origin, bytes(data)))

    receiver = Receiver()

    dispatcher = Dispatcher()
    options = UdpOptions()
    transport = UdpListenerTransport(dispatcher,
                                     ([multicast_group_1, multicast_group_2], multicast_port),
                                     receiver.on_received,
                                     DEFAULT_INTERFACE,
                                     options)

    # test publisher
    with socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP) as s:
        s.setsockopt(IPPROTO_IP, IP_MULTICAST_TTL, 0)

        # send test data
        send_buffer = bytearray([1, 2, 3, 4])
        s.sendto(send_buffer, multicast_address_and_port_1)

        send_buffer = bytearray([5, 6])
        s.sendto(send_buffer, multicast_address_and_port_2)

        assert_that(receiver.count, is_(0))

        dispatcher.dispatch()
        assert_that(receiver.count, is_(2))


def test_reception_and_read_times(multicast_address_and_port_1):
    class Receiver(object):
        def __init__(self):
            self.received = []

        def on_received(self, source, data):
            # type: (UdpListenerTransport, BufferRef) -> None
            self.received.append(source.info.since())

    receiver = Receiver()

    dispatcher = Dispatcher()
    options = UdpOptions()
    options.timestampns = True
    options.pktinfo = True
    transport = UdpListenerTransport(dispatcher,
                                     multicast_address_and_port_1,
                                     receiver.on_received,
                                     DEFAULT_INTERFACE,
                                     options)

    # test publisher
    with socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP) as s:
        s.setsockopt(IPPROTO_IP, IP_MULTICAST_TTL, 0)

        # send test data
        send_buffer = bytearray([1, 2, 3, 4])
        s.sendto(send_buffer, multicast_address_and_port_1)

        dispatcher.dispatch()

        assert_that(receiver.received, is_not(empty()))
        assert_that(receiver.received[0], is_not(None))
        assert_that(receiver.received[0], is_(instance_of(tuple)))
        assert_that(receiver.received[0][0], is_(instance_of(timedelta)))
        assert_that(receiver.received[0][1], is_(instance_of(timedelta)))


def test_resource_manager(multicast_group_1, multicast_port):
    dispatcher = Dispatcher()
    options = UdpOptions()
    with UdpListenerTransport(dispatcher,
                              (multicast_group_1, multicast_port),
                              lambda s, d: _,
                              DEFAULT_INTERFACE,
                              options) as transport:
        assert_that(bool(transport), is_(True))

    assert_that(bool(transport), is_(False))