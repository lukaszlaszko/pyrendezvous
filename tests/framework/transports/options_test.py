from framework.transports import UdpOptions, TcpOptions
from hamcrest import *


def test_create_udp_options__default():
    options = UdpOptions()
    assert_that(options, is_not(None))


def test_create_udp_options__kwargs():
    options = UdpOptions(read_buffer_size=1001,
                         send_buffer_size=1002,
                         sp_rcvbuf=1003,
                         sp_sndbuf=1004,
                         loop=True,
                         ttl=105,
                         timestampns=True,
                         pktinfo=True)
    assert_that(options.read_buffer_size, is_(equal_to(1001)))
    assert_that(options.send_buffer_size, is_(equal_to(1002)))
    assert_that(options.sp_rcvbuf, is_(equal_to(1003)))
    assert_that(options.sp_sndbuf, is_(equal_to(1004)))
    assert_that(options.loop, is_(True))
    assert_that(options.ttl, is_(equal_to(105)))
    assert_that(options.timestampns, is_(True))
    assert_that(options.pktinfo, is_(True))


def test_create_udp_options__unknown_arg():
    assert_that(calling(UdpOptions).with_args(unknown_arg='abc'), raises(ValueError))


def test_create_tcp_options__default():
    options = TcpOptions()
    assert_that(options, is_not(None))


def test_create_tcp_options__kwargs():
    options = TcpOptions(read_buffer_size=1001,
                         send_buffer_size=1002,
                         sp_rcvbuf=1003,
                         sp_sndbuf=1004,
                         listen_backlog=105,
                         no_delay=True,
                         quick_ack=True)
    assert_that(options.read_buffer_size, is_(equal_to(1001)))
    assert_that(options.send_buffer_size, is_(equal_to(1002)))
    assert_that(options.sp_rcvbuf, is_(equal_to(1003)))
    assert_that(options.sp_sndbuf, is_(equal_to(1004)))
    assert_that(options.listen_backlog, is_(equal_to(105)))
    assert_that(options.no_delay, is_(True))
    assert_that(options.quick_ack, is_(True))


def test_create_tcp_options__unknown_arg():
    assert_that(calling(TcpOptions).with_args(unknown_arg='abc'), raises(ValueError))


