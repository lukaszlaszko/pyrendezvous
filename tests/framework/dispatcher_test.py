from hamcrest import *
from framework import *


def test_create():
    def on_error(error):
        pass

    dispatcher = Dispatcher(on_error)


def test_create_default_on_error():
    dispatcher = Dispatcher()


def test_create_wrong_on_error():
    assert_that(calling(Dispatcher).with_args('abc', None),
                raises(TypeError))


def test_create_with_parent():
    def on_error(error):
        pass

    parent = Dispatcher(on_error)
    child = Dispatcher(on_error, parent)

