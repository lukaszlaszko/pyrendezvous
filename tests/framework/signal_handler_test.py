from hamcrest import *
from signal import SIGUSR1
from os import kill, getpid
from framework import *
import pytest


def is_debugged():
    try:
        import pydevd
        return True
    except ImportError:
        return False


class Receiver(object):
    def __init__(self):
        self.received_signo = 0
        self.error = None

    def on_signal(self, signo):
        self.received_signo = signo

    def on_error(self, error):
        self.error = error


def test_create_dispatcher_none():
    receiver = Receiver()

    assert_that(calling(SignalHandler).with_args(None, SIGUSR1, receiver.on_signal, receiver.on_error),
                raises(TypeError))


def test_create_signal_none():
    receiver = Receiver()

    dispatcher = Dispatcher()
    assert_that(calling(SignalHandler).with_args(dispatcher, None, receiver.on_signal, receiver.on_error),
                raises(TypeError))


def test_create_signal_callback_none():
    receiver = Receiver()

    dispatcher = Dispatcher()
    assert_that(calling(SignalHandler).with_args(dispatcher, SIGUSR1, None, receiver.on_error),
                raises(TypeError))


def test_create_error_callback_default():
    receiver = Receiver()

    dispatcher = Dispatcher()
    sigusr1_handler = SignalHandler(dispatcher, SIGUSR1, receiver.on_signal)


@pytest.mark.skipif(is_debugged(), reason="kill(getpid(), SIGUSR1) shutsdown debugger")
def test_handle_signal():
    receiver = Receiver()

    dispatcher = Dispatcher()
    sigusr1_handler = SignalHandler(dispatcher, SIGUSR1, receiver.on_signal, receiver.on_error)

    assert_that(receiver.received_signo, is_(0))
    assert_that(receiver.error, is_(None))

    kill(getpid(), SIGUSR1)

    assert_that(receiver.received_signo, is_(0))
    assert_that(receiver.error, is_(None))

    dispatcher.dispatch()

    assert_that(receiver.received_signo, is_(SIGUSR1))
    assert_that(receiver.error, is_(None))
